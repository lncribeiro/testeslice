<body>
    <div class="container">
      <header class="blog-header py-3">
      </header>

      <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">Teste de Desenvolvimento</h1>
          <p class="lead my-3">Teste PHP/JS/HTML para correção de itens com problemas, existe uma imagem que contém exatamente como deve ficar. Procure a imagem na pasta /img/layout/layout.jpg.  Todos os Links devem estar funcionando .</p>
        </div>
      </div>

<div class="row mb-2">
  <?php
    $stringJson = file_get_contents("json/posts.json");
    $jsonPost = json_decode($stringJson, TRUE);     
    
    $decode = json_decode( $stringJson, TRUE );
	foreach ( $decode["posts"] as $valor){
        echo '<div class="col-md-6">'.PHP_EOL;
        echo '<div class="card flex-md-row mb-4 box-shadow h-md-250">'.PHP_EOL;
        echo '<div class="card-body d-flex flex-column align-items-start">'.PHP_EOL;
		echo '<strong class="'.$valor["chapeu"].'st">'.$valor["chapeu"].'</strong>'.PHP_EOL;
        echo '<h3 class="mb-0">'.PHP_EOL;
        echo '<a class="text-dark" href="'.$valor["link"].'">'.$valor["titulo"].'</a>'.PHP_EOL;
		echo '</h3>'.PHP_EOL;
        echo '<p class="card-text mb-auto">'.$valor["linhafina"].'</p>'.PHP_EOL;
		echo '</div></div></div>'.PHP_EOL;
	}
    
    
    ?>

          </div>
      </div>
       <?php
      include 'footer/index.php'
    ?>

   
</body>

        
    